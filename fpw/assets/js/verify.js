var verify = $('#verify');
var verify2 = $('#verify2');
var password = $("#password");
var password2 = $('#password2');
var url_string = window.location.href;
var url = new URL(url_string);
var t = url.searchParams.get("t");
var m = url.searchParams.get("m");


var f = {
  "email": m,
  "token": t
};
var submit = $("#update");

var passbox2 = $('#passboxpassword2');

password.on('keyup', function(){

    let field_value = password.val();
    let field_length = field_value.length;
    let passbox = $('#passboxpassword');

    if(field_length > 5){
        if(field_value.match(/[a-z]/) && field_value.match(/[A-Z]/) && field_value.match(/\d/)){
            verify.html('* Strong and valid');
            verify.addClass('text-success');
            verify.removeClass('text-warning');
            verify.removeClass('text-danger');
            passbox.css('border', '1px solid #28a745');
            passbox.css('border-radius', '.25rem');
            passbox.css('box-shadow', '0 0 0 0.1rem rgba(40,167,69,.5)');
        }else{
            verify.html('* Valid');
            verify.addClass('text-warning');
            verify.removeClass('text-success');
            verify.removeClass('text-danger');
            passbox.css('border', '1px solid #ffc107');
            passbox.css('border-radius', '.25rem');
            passbox.css('box-shadow', '0 0 0 0.1rem rgba(255,193,7,.5)');
        }
    }else{
        verify.html('*please enter at least 6 digits');
        verify.addClass('text-danger');
        verify.removeClass('text-warning');
        verify.removeClass('text-success');
        passbox.css('border', '1px solid #dc3545');
        passbox.css('border-radius', '.25rem');
        passbox.css('box-shadow', '0 0 0 0.1rem rgba(220,53,69,.5)');
    }

    password2.trigger('keyup');

});

password2.on('keyup', function(){

    let field_value = password2.val();
    let compare_value = password.val();
    let compare_length = compare_value.length;
    let passbox = $('#passboxpassword2');


    if(compare_length > 0 && field_value.toString() == compare_value.toString()){
        submit.removeClass('disabled');
        verify2.html('* Perfect match');
        verify2.addClass('text-success');
        verify2.removeClass('text-danger');
        passbox.css('border', '1px solid #28a745');
        passbox.css('border-radius', '.25rem');
        passbox.css('box-shadow', '0 0 0 0.1rem rgba(40,167,69,.5)');
        submit.removeClass('disabled');
      }else if (field_value.length < 1) {
        passbox.css('border', '1px solid #ced4da');
        passbox.css('border-radius', '.25rem');
        verify2.removeClass('text-success');
        verify2.html('');
        passbox.css('box-shadow', '0 0 0 0.1rem rgba(0, 0, 0, 0.36)');
    }else{
        submit.addClass('disabled');
        verify2.html('*passwords dont match');
        verify2.addClass('text-danger');
        verify2.removeClass('text-success');
        passbox.css('border', '1px solid #dc3545');
        passbox.css('border-radius', '.25rem');
        passbox.css('box-shadow', '0 0 0 0.1rem rgba(220,53,69,.5)');
    }

});

$(document).ready(function() {
  $('.showbox').hide();
});

submit.on('click',function(event) {
  if(!$('#update').hasClass('disabled')){
    let requestBody = new Object();
    requestBody.email = f.email;
    requestBody.token = f.token;
    requestBody.password = $(password2).val();
    $('.showbox').show();
    $('#update').hide();
    $.post("https://5mbnsmuk4j.execute-api.eu-west-1.amazonaws.com/dev/appuser/validatepassword",JSON.stringify(requestBody),function(data){
      // Success
      $('.response-message').html("Your Password has been successfully updated");
      $('.response-message').addClass('text-success');
      $('.showbox').hide();
      console.log(data);
    }).fail(function(data){
      //Fail
      $('.response-message').html("Your Password has failed to update");
      $('.response-message').addClass('text-danger');
      console.log(data);
    })
  }

})
