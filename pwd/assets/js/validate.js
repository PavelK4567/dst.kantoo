function pwd_validate() {

	var url_string = window.location.href;
	var url = new URL(url_string);
	var t = url.searchParams.get("t");
	var m = url.searchParams.get("m");


	var f = {
		"email": m,
		"token": t
	};

	$.post( "https://6avc2hj4wb.execute-api.eu-west-1.amazonaws.com/prod/validate", JSON.stringify(f) , function( data ) {
		var s = JSON.stringify(data);
		console.log(s);
		console.log(data);
		if (s=="NO_ERROR" || data=="NO_ERROR") {
		    $('#response-content').html('<h2 class="standard mt-2">Congratulations!</h2><h2 class="standard mt-2 mb-4">Your password is sent to your email</h2>');
		} else {
		    $('#response-content').html('<h2 class="standard mt-2">Password reset failed</h2><h2 class="standard mt-2 mb-4">There was an error in your password reset request</h2>');
		}
	});

}

pwd_validate();