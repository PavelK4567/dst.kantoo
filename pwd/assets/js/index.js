function readTypes() {
    $.getJSON( "https://s3-eu-west-1.amazonaws.com/ireland-prod-published/lmdisney-pwd/products.json", function( data ) {
        var $select = $('#select_product_type'); 
        $select.find('option').remove();
        $select.append('<option value=\'\'></option>');
        $.each(data,function(key, value){
            var n = encodeURIComponent(value.name);
            $select.append('<option value=' + n + '>' + value.title + '</option>');
        });

    });
}
    
readTypes();

function pwd_reset() {
    var e = document.getElementById("email").value;
    //var t = document.getElementById("select_product_type").value;
    var t = $('#select_product_type').find(":selected").val();
    
    t = decodeURIComponent(t);

    if(e !== '' && typeof e !== 'undefined' && t !== '' && t !== "undefined" && typeof t !== 'undefined'){
        var f = {
            "email": e,
            "productID": t
        };

        $.post( "https://6avc2hj4wb.execute-api.eu-west-1.amazonaws.com/prod/reset", JSON.stringify(f) , function( data ) {
            var s = JSON.stringify(data);
            console.log(s);
            if (data.isEmailWasSent=="1" || data.isEmailWasSent==1) {
                $('#feedback').html('Password reset instructions sent to your email.');
                $('#feedback').addClass('text-success');
                $('#feedback').removeClass('text-danger');
            } else {
                $('#feedback').html('Error sending password reset instructions. Please check your details and try again.');
            }
        });
        ;
    }else{
        $('#feedback').html('Please complete both fields to continue');
        return;
    }

    

}