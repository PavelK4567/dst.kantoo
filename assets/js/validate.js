function email_validate() {

	var url_string = window.location.href;
	var url = new URL(url_string);
	var t = url.searchParams.get("t");
	var m = url.searchParams.get("m");


	var f = {
		"email": m,
		"token": t
	};

	$.post( "https://6avc2hj4wb.execute-api.eu-west-1.amazonaws.com/prod/validate", JSON.stringify(f) , function( data ) {
		var s = JSON.stringify(data);
		console.log(s);
		console.log(data);
		if (s=="NO_ERROR" || data=="NO_ERROR") {
		    window.location = "email-success.html";
		} else {
		    window.location = "email-error.html";
		}
	});

}

email_validate();