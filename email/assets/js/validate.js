//import {DOMAIN} from '../../../links.js';
const DOMAIN = "https://prod.b2c.api.kantoo.com/"
const validateUrl = DOMAIN + "appuser/validate";

const NO_ERROR = 0;
const EMAIL_ALREADY_VALIDATED = 1;
const TOKEN_EXPIRED = 3;
const TOKEN_NOT_ACTIVE = 4;


function email_validate() {
    console.log("Running email validation ... ");
    const url_string = window.location.href;
    const url = new URL(url_string);
    const email = url.searchParams.get("m");
    const token = url.searchParams.get("t");
    const language = url.searchParams.get("l");

    const requestBody = {
        "email": email,
        "token": token
    };


    console.log(`Sending request to ${validateUrl} with input ${JSON.stringify(requestBody)}`);

    $.post(validateUrl, requestBody , serverResponse => {
        console.log(`Server validation response = ${JSON.stringify(serverResponse)}`);
        const statusCode = serverResponse.statusCode;

        if (statusCode === EMAIL_ALREADY_VALIDATED) {
            window.location = "email-already-validated.html";
        } else if (statusCode === TOKEN_EXPIRED) {
            console.log(serverResponse.message);
            window.location = "email-token-expired.html";
        } else if (statusCode === TOKEN_NOT_ACTIVE) {
            console.log(serverResponse.message);
            window.location = "email-not-active.html";
        } else if (statusCode === NO_ERROR) {
            window.location = "email-success.html?l="+language;
        } else {
            alert(serverResponse.message);
        }
    });

}

email_validate();
