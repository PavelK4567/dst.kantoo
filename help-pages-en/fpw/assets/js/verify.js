var verify = $('#verify');
var verify2 = $('#verify2');
var password = $("#password");
var password2 = $('#password2');
var url_string = window.location.href;
var url = new URL(url_string);
var t = url.searchParams.get("t");
var m = url.searchParams.get("m");


var f = {
  "email": m,
  "token": t
};
var submit = $("#update");

var passbox2 = $('#passboxpassword2');

password.on('keyup', function(){

    let field_value = password.val();
    let field_length = field_value.length;
    let passbox = $('#passboxpassword');

    if(field_length > 5){
        if(field_value.match(/[a-z]/) && field_value.match(/[A-Z]/) && field_value.match(/\d/)){
            verify.html('* Strong and valid');
            verify.addClass('text-success');
            verify.removeClass('text-warning');
            verify.removeClass('text-danger');
            passbox.css('border', '1px solid #28a745');
            passbox.css('border-radius', '.25rem');
            passbox.css('box-shadow', '0 0 0 0.1rem rgba(40,167,69,.5)');
        }else{
            verify.html('* Valid');
            verify.addClass('text-warning');
            verify.removeClass('text-success');
            verify.removeClass('text-danger');
            passbox.css('border', '1px solid #ffc107');
            passbox.css('border-radius', '.25rem');
            passbox.css('box-shadow', '0 0 0 0.1rem rgba(255,193,7,.5)');
        }
    }else{
        verify.html('*please enter at least 6 digits');
        verify.addClass('text-danger');
        verify.removeClass('text-warning');
        verify.removeClass('text-success');
        passbox.css('border', '1px solid #dc3545');
        passbox.css('border-radius', '.25rem');
        passbox.css('box-shadow', '0 0 0 0.1rem rgba(220,53,69,.5)');
    }

    password2.trigger('keyup');

});

password2.on('keyup', function(){

    let field_value = password2.val();
    let compare_value = password.val();
    let compare_length = compare_value.length;
    let passbox = $('#passboxpassword2');

    if(compare_length > 0 && field_value.toString() == compare_value.toString()){
        submit.removeClass('disabled');
        verify2.html('* Perfect match');
        verify2.addClass('text-success');
        verify2.removeClass('text-danger');
        $('.input-group').css('border', '1px solid #28a745');
        $('.input-group').css('border-radius', '.25rem');
        $('.input-group').css('box-shadow', '0 0 0 0.1rem rgba(40,167,69,.5)');
        submit.removeClass('disabled');
    }else{
        submit.addClass('disabled');
        verify2.html('*passwords dont match');
        verify2.addClass('text-danger');
        verify2.removeClass('text-success');
        $('.input-group').css('border', '1px solid #dc3545');
        $('.input-group').css('border-radius', '.25rem');
        $('.input-group').css('box-shadow', '0 0 0 0.1rem rgba(220,53,69,.5)');
    }

});

$(document).ready(function() {
  $('.showbox').hide();
});

submit.on('click',function(event) {
  if(!$('#update').hasClass('disabled')){
  let requestBody = new Object();
  requestBody.email = f.email;
  requestBody.token = f.token;
  requestBody.password = $(password2).val();
  $('.showbox').show();
  $('#update').hide();


  $.ajax({
    url: 'https://prod.b2c.api.kantoo.com/appuser/validatepassword',
    type: 'post',
    data: {
      email:  f.email,
      token: f.token,
      password: $(password2).val()
    },
    headers: {
      Content_Type: 'application/json'
    },
    dataType: 'json',
    success: function(data){
      // Success
      if(data.status == "Bad Request"){
        $('.response-message').html("Your Password has failed to update");
        $('.response-message').addClass('text-danger');
      }else{
        $('.response-message').html("Your Password has been successfully updated");
        $('.response-message').addClass('text-success');
        $('.showbox').hide();
      }

      console.log(data);
    },
    fail: function(data){
      //Fail
      $('.response-message').html("Your Password has failed to update");
      $('.response-message').addClass('text-danger');
      console.log(data);
    }
  })


/*  $.post("https://prod.b2c.api.kantoo.com/appuser/validatepassword",JSON.stringify(requestBody),function(data){
    // Success
    if(data.status == "Bad Request"){
      $('.response-message').html("Your Password has failed to update");
      $('.response-message').addClass('text-danger');
    }else{
      $('.response-message').html("Your Password has been successfully updated");
      $('.response-message').addClass('text-success');
      $('.showbox').hide();
    }

    console.log(data);
  }).fail(function(data){
    //Fail
    $('.response-message').html("Your Password has failed to update");
    $('.response-message').addClass('text-danger');
    console.log(data);
  })*/
}
})
