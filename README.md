# Mobile help pages

Mobile web site for serving FAQ, privacy policy, and terms & conditions , etc. for DST App

The page is able to redirect the user to the relevant language / operator using the following query string:

lang = [es/pt]
operator = [vivo(pt)/claro(es)]

#example:

[address]?lang=es/pt

[address]?operator=vivo/claro

[address]?lang=es/pt&operator=vivo/claro
